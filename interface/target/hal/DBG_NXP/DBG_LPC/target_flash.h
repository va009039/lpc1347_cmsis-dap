/* CMSIS-DAP Interface Firmware
 * Copyright (c) 2009-2013 ARM Limited
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
#ifndef TARGET_FLASH_H
#define TARGET_FLASH_H

#include "target_struct.h"
#include "swd_host.h"
#include <stdint.h>

#define USE_MAC_SAFARI 1
#define USE_COMPARE_VERIFY 0
#define USE_BLANK_CHECK_VERIFY 0
#define USE_LPCXPRESSO 1

#define SET_VALID_CODE 1       // Set Valid User Code Signature

/* Code Read Protection (CRP) */
#define CRP_ADDRESS (0x000002FC)
#define NO_ISP      (0x4E697370)
#define CRP1        (0x12345678)
#define CRP2        (0x87654321)
#define CRP3        (0x43218765)
#define IS_CRP_VALUE(v) ((v==NO_ISP) || (v==CRP1) || (v==CRP2) || (v==CRP3))
#define NO_CRP      (1)     /* Forbid programming if CRP is enabled. */

#define FLASH_SECTOR_SIZE           (1024)

#define TARGET_AUTO_INCREMENT_PAGE_SIZE    (0x1000)

static uint8_t target_flash_init(uint32_t clk);
static uint8_t target_flash_uninit(void);
static uint8_t target_flash_erase_chip(void);
static uint8_t target_flash_erase_sector(uint32_t sector);
static uint8_t target_flash_program_page(uint32_t adr, uint8_t * buf, uint32_t size);

#define RAM_START 0x10000000
#define RAM_SIZE 0x400
#define PARAM_BUF RAM_START
#define PARAM_BUF_SIZE 44
#define PROG_BUF  (RAM_START+PARAM_BUF_SIZE)
#define PROG_BUF_SIZE 512
#define RAM_END   (RAM_START+RAM_SIZE)

static FLASH_SYSCALL sys_call_param = {(PARAM_BUF+1), 0, RAM_END-32}; // breakpoint, RSB, RSP

// LPC1347,LPC1114,LPC812,LPC810
#define LPC_SYSCON_SYSMEMREMAP  0x40048000
#define LPC_SYSCON_MAINCLKSEL   0x40048070
#define LPC_SYSCON_MAINCLKUEN   0x40048074
#define LPC_SYSCON_SYSAHBCLKDIV 0x40048078

// LPC1768,LPC1769
#define LPC_SC_MEMMAP   0x400fc040
#define LPC_SC_PLL0CON  0x400fc080
#define LPC_SC_PLL0CFG  0x400fc084
#define LPC_SC_PLL0STAT 0x400fc088
#define LPC_SC_PLL0FEED 0x400fc08c
#define LPC_SC_CCLKCFG  0x400FC104
#define LPC_SC_USBCLKCFG 0x400FC108
#define LPC_SC_CLKSRCSEL 0x400FC10C
#define LPC_SC_SCS       0x400FC1A0

#define PLLCON_PLLE      (1<<0)
#define PLLCON_PLLC      (1<<1)
#define PLLSTAT_LOCK     (1<<26)
#define SCS_OSCEN        (1<<5)
#define SCS_OSCSTAT      (1<<6)

enum IAP_CMD {
    PREPARE_SECTOR = 50,
    COPY_RAM_TO_FLASH = 51,
    ERASE_SECTOR = 52,
    BLANK_CHECK = 53,
    READ_PART_ID = 54,
    READ_BOOT_CODE_VERSION = 55,
    COMPARE = 56,
    REINVOKE_ISP = 57,
    READ_UID = 58,
    ERASE_PAGE = 59,
};

enum IAP_STATUS {
    CMD_SUCCESS = 0,
    INVALID_COMMAND = 1,
    SRC_ADDR_ERROR = 2,
    DST_ADDR_ERROR = 3,
    SRC_ADDR_NOT_MAPPED = 4,
    DST_ADDR_NOT_MAPPED = 5,
    COUNT_ERROR = 6,
    INVALID_SECTOR = 7,
    SECTOR_NOT_BLANK = 8,
    SECTOR_NOT_PREPARED_FOR_WRITE_OPERATION = 9,
    COMPARE_ERROR = 10,
    BUSY = 11,
    ADDR_ERROR = 13,

    IAP_STAT_ERROR = 255,
};

static struct {
    struct {
        uint32_t bkpt;   // +0
    } code;
    struct {             // IAP Structure
        uint32_t cmd;    // +4 Command
        uint32_t par[4]; // +8 Parameters
        uint32_t stat;   // +24 Status
        uint32_t res[4]; // +28 Result
    } IAP;               // +44
} ram; 

static uint32_t part_id;

static struct {
    uint32_t iap_cclk;
    uint32_t sector_size;
} cfg = {
    12000, // 12Mhz
    4096,
};

static uint8_t remoteIAP(uint32_t cmd, uint32_t p0, uint32_t p1, uint32_t p2, uint32_t p3) {
    ram.code.bkpt = 0xe00abe00; // bkpt #00
    ram.IAP.cmd = cmd;
    ram.IAP.par[0] = p0;
    ram.IAP.par[1] = p1;
    ram.IAP.par[2] = p2;
    ram.IAP.par[3] = p3;
    ram.IAP.stat = IAP_STAT_ERROR;

    if (!swd_write_memory(PARAM_BUF, (uint8_t*)&ram, 28)) {
        return 0;
    }
    swd_flash_syscall_exec(&sys_call_param, 0x1fff1ff1, PARAM_BUF+4, PARAM_BUF+24, 0, 0);
    if (cmd == READ_PART_ID) {
        if (!swd_read_memory(PARAM_BUF+24, (uint8_t*)&ram.IAP.stat, 4*2)) {
            return 0;
        }
        part_id = ram.IAP.res[0];
    } else {
        if (!swd_read_memory(PARAM_BUF+24, (uint8_t*)&ram.IAP.stat, 4*1)) {
            return 0;
        }
    }
    if (ram.IAP.stat != CMD_SUCCESS) {
        return 0;
    }
    return 1;
}

static uint8_t remoteREG(uint32_t addr, uint32_t value) {
    if (!swd_write_memory(addr, (uint8_t*)&value, 4)) {
        return 0;
    }
    return 1;
}

static uint32_t remoteREG_READ(uint32_t addr) {
    uint32_t data;
    swd_read_memory(addr, (uint8_t*)&data, 4);
    return data;
}

static uint8_t remoteREG_IAND(uint32_t addr, uint32_t value) {
    uint32_t data;
    swd_read_memory(addr, (uint8_t*)&data, 4);
    data &= value;
    swd_write_memory(addr, (uint8_t*)&data, 4);
    return 1;
}

static uint8_t remoteREG_IOR(uint32_t addr, uint32_t value) {
    uint32_t data;
    swd_read_memory(addr, (uint8_t*)&data, 4);
    data |= value;
    swd_write_memory(addr, (uint8_t*)&data, 4);
    return 1;
}

static uint8_t flash_init_lpc1347() {
    cfg.iap_cclk = 12000;
    cfg.sector_size = 4096;
    remoteREG(LPC_SYSCON_MAINCLKSEL, 0); // Select Internal RC Oscillator
    remoteREG(LPC_SYSCON_SYSAHBCLKDIV, 1);// Set Main Clock divider to 1
    if (!remoteREG(LPC_SYSCON_SYSMEMREMAP, 2)) { // User Flash Mode
        return 0;
    }
    return 1;
}

static uint8_t flash_init_lpc1769() {
    cfg.sector_size = 4096;
#if USE_LPCXPRESSO
    /* Setup PLL etc. to give CCLK = 60MHz */
    cfg.iap_cclk = 60000;
    /* Shut down PLL */
    remoteREG_IAND(LPC_SC_PLL0CON, ~PLLCON_PLLC); /* Disconnect */
    remoteREG(LPC_SC_PLL0FEED, 0xAA);
    remoteREG(LPC_SC_PLL0FEED, 0x55);
    remoteREG_IAND(LPC_SC_PLL0CON, ~PLLCON_PLLE); /* Disable */
    remoteREG(LPC_SC_PLL0FEED, 0xAA);
    remoteREG(LPC_SC_PLL0FEED, 0x55);

    /* Start main oscillator */
    remoteREG_IOR(LPC_SC_SCS, SCS_OSCEN);
    while ((remoteREG_READ(LPC_SC_SCS) & SCS_OSCSTAT) == 0);

    /* Select main oscillator */
    remoteREG(LPC_SC_CLKSRCSEL, 0x01);

    /* Configure PLL */
    remoteREG(LPC_SC_PLL0CFG, 0x00000013);
    remoteREG(LPC_SC_PLL0FEED, 0xAA);
    remoteREG(LPC_SC_PLL0FEED, 0x55);

    /* Enable PLL */
    remoteREG_IOR(LPC_SC_PLL0CON, PLLCON_PLLE);
    remoteREG(LPC_SC_PLL0FEED, 0xAA);
    remoteREG(LPC_SC_PLL0FEED, 0x55);
    while ((remoteREG_READ(LPC_SC_PLL0STAT) & PLLSTAT_LOCK) == 0);

    /* Set CPU clock divider */
    remoteREG(LPC_SC_CCLKCFG, 0x00000007);

    /* Set USB clock divider (just to set this clock to a reasonable value) */
    remoteREG(LPC_SC_USBCLKCFG, 0x00000009);

    /* Connect PLL */
    remoteREG_IOR(LPC_SC_PLL0CON, PLLCON_PLLC);
    remoteREG(LPC_SC_PLL0FEED, 0xAA);
    remoteREG(LPC_SC_PLL0FEED, 0x55);

    /* Select user flash mode */
    if (!remoteREG(LPC_SC_MEMMAP, 0x01)) {
        return 0;
    }
    return 1;
#else
    cfg.iap_cclk = 4000;
    remoteREG(LPC_SC_PLL0CON, 0x00); // Select Internal RC Oscillator
    remoteREG(LPC_SC_PLL0FEED, 0xaa);
    remoteREG(LPC_SC_PLL0FEED, 0x55);
    if (!remoteREG(LPC_SC_MEMMAP, 0x01)) { // User Flash Mode
        return 0;
    }
    return 1;
#endif
}

static uint8_t flash_init_lpc1114fn28_lpc810(uint32_t sector_size) {
    int i;
    uint32_t data;

    cfg.iap_cclk = 12000;
    cfg.sector_size = sector_size;
    remoteREG(LPC_SYSCON_MAINCLKSEL, 0); // Select Internal RC Oscillator
    remoteREG(LPC_SYSCON_MAINCLKUEN, 1); // Update Main Clock Source
    remoteREG(LPC_SYSCON_MAINCLKUEN, 0); // Toggle Update Register
    remoteREG(LPC_SYSCON_MAINCLKUEN, 1);
    // Wait until updated
    for(i = 0; i < 100; i++) {
        if (!swd_read_memory(LPC_SYSCON_MAINCLKUEN, (uint8_t*)&data, 4)) {
            return 0;
        }
        if (data & 1) {
            break;
        }
    }
    remoteREG(LPC_SYSCON_SYSAHBCLKDIV, 1);// Set Main Clock divider to 1
    if (!remoteREG(LPC_SYSCON_SYSMEMREMAP, 2)) { // User Flash Mode
        return 0;
    }
    return 1;}

static uint8_t target_flash_init(uint32_t clk) {
    if (!remoteIAP(READ_PART_ID, 0, 0, 0, 0)) {
        return 0;
    }
    if (part_id == 0x08020543) { // LPC1347 ?
        return flash_init_lpc1347();
    }
    if (part_id == 0x26113f37 || part_id == 0x26013f37) { // LPC1769 or LPC1768 ?
        return flash_init_lpc1769();
    }
    if (part_id >= 0x00008100 && part_id <= 0x00008122) { // LPC812 or LPC810 ?
        return flash_init_lpc1114fn28_lpc810(1024);
    }
    // part_id = 0x0a40902b or 0x1a40902b
    return flash_init_lpc1114fn28_lpc810(4096); // LPC1114FN28
}

static uint8_t erase_sector_by_sector(uint32_t sector) {
    if (!remoteIAP(PREPARE_SECTOR, sector, sector, 0, 0)) {
        return 0;
    }
    if (!remoteIAP(ERASE_SECTOR, sector, sector, cfg.iap_cclk, 0)) {
        return 0;
    }
#if USE_BLANK_CHECK_VERIFY == 1
    if (!remoteIAP(BLANK_CHECK, sector, sector, cfg.iap_cclk, 0)) {
        return 0;
    }
#endif
    return 1;
}

static uint8_t target_flash_erase_sector(unsigned int sector) {
#if USE_MAC_SAFARI == 1
    return erase_sector_by_sector(sector);
#else
    return 0;
#endif
}

static uint8_t target_flash_erase_chip(void) {
    return erase_sector_by_sector(0);
}

static uint32_t addr_to_sector(uint32_t addr) {
    if (addr < 0x10000) {
        return addr / cfg.sector_size;
    }
    return addr / 0x8000 + 0x10000/cfg.sector_size - 2;
}

static uint8_t sector_head(uint32_t addr) {
    if (addr < 0x10000) {
        return (addr%cfg.sector_size) == 0;
    }
    return (addr%0x8000) == 0;
}

static uint8_t target_flash_program_page(uint32_t addr, uint8_t * buf, uint32_t size) {
    const int chunk_size = PROG_BUF_SIZE;
    uint32_t n;
    uint32_t i;
    uint32_t sector;

#if NO_CRP != 0
    if (addr == 0) {
        n = *((uint32_t*)(buf + CRP_ADDRESS));
        if (IS_CRP_VALUE(n)) {
            return 0; /* CRP is enabled, exit. */
        }
    }
#endif

#if SET_VALID_CODE != 0       // Set valid User Code Signature
    if (addr == 0) {          // Check for Vector Table
        n = 0;
        for(i = 0x00; i <= 0x18; i += 4) {
            n += *((uint32_t*)(buf + i));
        }
        *((uint32_t*)(buf + 0x1C)) = 0 - n; // Signature at Reserved Vector
    }
#endif
    // Program a page in target flash.
    for(i = 0; i < size; i += chunk_size) {
        if (!swd_write_memory(PROG_BUF, buf+i, chunk_size)) {
            return 0;
        }
        sector = addr_to_sector(addr);
        if (sector_head(addr)) {
            if (!erase_sector_by_sector(sector)) {
                return 0;
            }
        }
        if (!remoteIAP(PREPARE_SECTOR, sector, sector, 0, 0)) {
            return 0;
        }
        if (!remoteIAP(COPY_RAM_TO_FLASH, addr, PROG_BUF, chunk_size, cfg.iap_cclk)) {
            return 0;
        }
#if USE_COMPARE_VERIFY == 1
        if (!remoteIAP(COMPARE, addr, PROG_BUF, chunk_size, 0)) {
            return 0;
        }
#endif
        addr += chunk_size;
    }
    return 1;
}

#endif
