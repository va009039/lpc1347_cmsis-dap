/* CMSIS-DAP Interface Firmware
 * Copyright (c) 2009-2013 ARM Limited
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
#if defined(TARGET_MKL25Z)
#include <MKL25Z4.h>
#else
#error "Unknown target type"
#endif
#include <RTL.h>
#include "gpio.h"

static U16 isr_flags;
static OS_TID isr_notify;

// DAP_LED PTB18
#define PIN_DAP_LED_PORT  PORTB
#define PIN_DAP_LED_GPIO  PTB
#define PIN_DAP_LED_BIT   18
#define PIN_DAP_LED       (1<<PIN_DAP_LED_BIT)

// MSD_LED PTB19
#define PIN_MSD_LED_PORT  PORTB
#define PIN_MSD_LED_GPIO  PTB
#define PIN_MSD_LED_BIT   19
#define PIN_MSD_LED       (1<<PIN_MSD_LED_BIT)

// CDC_LED PTD1
#define PIN_CDC_LED_PORT  PORTD
#define PIN_CDC_LED_GPIO  PTD
#define PIN_CDC_LED_BIT   1
#define PIN_CDC_LED       (1<<PIN_CDC_LED_BIT)

// SW RESET BUTTON PTD3
#define PIN_SW_RESET_PORT PORTD
#define PIN_SW_RESET_GPIO PTD
#define PIN_SW_RESET_BIT  3
#define PIN_SW_RESET      (1<<PIN_SW_RESET_BIT)
#define PIN_SW_RESET_PORT_IRQn PORTD_IRQn
#define PIN_SW_RESET_PORT_IRQHandler PORTD_IRQHandler


void gpio_init(void)
{
    // enable clock PORT
    if (PIN_DAP_LED_PORT == PORTA || PIN_MSD_LED_PORT == PORTA || PIN_CDC_LED_PORT == PORTA) {
        SIM->SCGC5 |= SIM_SCGC5_PORTA_MASK;
    }
    if (PIN_DAP_LED_PORT == PORTB || PIN_MSD_LED_PORT == PORTB || PIN_CDC_LED_PORT == PORTB) {
        SIM->SCGC5 |= SIM_SCGC5_PORTB_MASK;
    }
    if (PIN_DAP_LED_PORT == PORTC || PIN_MSD_LED_PORT == PORTC || PIN_CDC_LED_PORT == PORTC) {
        SIM->SCGC5 |= SIM_SCGC5_PORTC_MASK;
    }
    if (PIN_DAP_LED_PORT == PORTD || PIN_MSD_LED_PORT == PORTD || PIN_CDC_LED_PORT == PORTD) {
        SIM->SCGC5 |= SIM_SCGC5_PORTD_MASK;
    }
    if (PIN_DAP_LED_PORT == PORTE || PIN_MSD_LED_PORT == PORTE || PIN_CDC_LED_PORT == PORTE) {
        SIM->SCGC5 |= SIM_SCGC5_PORTE_MASK;
    }
    // configure pin as GPIO
    PIN_DAP_LED_PORT->PCR[PIN_DAP_LED_BIT] = PORT_PCR_MUX(1);
    PIN_MSD_LED_PORT->PCR[PIN_MSD_LED_BIT] = PORT_PCR_MUX(1);
    PIN_CDC_LED_PORT->PCR[PIN_CDC_LED_BIT] = PORT_PCR_MUX(1);

    // led off - enable output
    PIN_DAP_LED_GPIO->PSOR = PIN_DAP_LED;
    PIN_MSD_LED_GPIO->PSOR = PIN_MSD_LED;
    PIN_CDC_LED_GPIO->PSOR = PIN_CDC_LED;

    PIN_DAP_LED_GPIO->PDDR |= PIN_DAP_LED;
    PIN_MSD_LED_GPIO->PDDR |= PIN_MSD_LED;
    PIN_CDC_LED_GPIO->PDDR |= PIN_CDC_LED;
}

void gpio_set_dap_led(uint8_t state)
{
    if (!state) {
        PIN_DAP_LED_GPIO->PCOR = PIN_DAP_LED; // LED on
    } else {
        PIN_DAP_LED_GPIO->PSOR = PIN_DAP_LED; // LED off
    }
}

void gpio_set_cdc_led(uint8_t state)
{
    if (!state) {
        PIN_CDC_LED_GPIO->PCOR = PIN_CDC_LED; // LED on
    } else {
        PIN_CDC_LED_GPIO->PSOR = PIN_CDC_LED; // LED off
    }
}

void gpio_set_msd_led(uint8_t state)
{
    if (!state) {
        PIN_MSD_LED_GPIO->PCOR = PIN_MSD_LED; // LED on
    } else {
        PIN_MSD_LED_GPIO->PSOR = PIN_MSD_LED; // LED off
    }
}

uint8_t GPIOGetButtonState(void)
{
    return 0;
}

void gpio_enable_button_flag(OS_TID task, U16 flags)
{
    // When the "reset" button is pressed the ISR will set the
    // event flags "flags" for task "task"
    // Keep a local copy of task & flags
    isr_notify=task;
    isr_flags=flags;

    // enable clock PORT
    if (PIN_SW_RESET_PORT == PORTA) {
        SIM->SCGC5 |= SIM_SCGC5_PORTA_MASK;
    }
    if (PIN_SW_RESET_PORT == PORTD) {
        SIM->SCGC5 |= SIM_SCGC5_PORTD_MASK;
    }

    PIN_SW_RESET_PORT->PCR[PIN_SW_RESET_BIT] |= PORT_PCR_ISF_MASK;
    //sw2 - interrupt on falling edge
    PIN_SW_RESET_PORT->PCR[PIN_SW_RESET_BIT] = PORT_PCR_PS_MASK /* Pull-up */
                                              |PORT_PCR_PE_MASK /* Pull enable */
                                              |PORT_PCR_PFE_MASK
                                              |PORT_PCR_IRQC(10) /* IRQ_FALLING_EDGE */
                                              |PORT_PCR_MUX(1); /* GPIO */

    NVIC_ClearPendingIRQ(PIN_SW_RESET_PORT_IRQn);
    NVIC_EnableIRQ(PIN_SW_RESET_PORT_IRQn);
}

void PIN_SW_RESET_PORT_IRQHandler(void)
{
    if(PIN_SW_RESET_PORT->ISFR & PIN_SW_RESET) {
        PIN_SW_RESET_PORT->PCR[PIN_SW_RESET_BIT] |= PORT_PCR_ISF_MASK;
        // Notify a task that the button has been pressed
        // disable interrupt
        PIN_SW_RESET_PORT->PCR[PIN_SW_RESET_BIT] = PORT_PCR_PS_MASK  /* Pull-up */
                                                  |PORT_PCR_PE_MASK  /* Pull enable */
                                                  |PORT_PCR_PFE_MASK /* IRQ Falling edge */
                                                  |PORT_PCR_IRQC(00) /* IRQ_DISABLED */
                                                  |PORT_PCR_MUX(1);  /* GPIO */

        isr_evt_set(isr_flags, isr_notify);
    }
}

